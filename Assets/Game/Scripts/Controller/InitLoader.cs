using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InitLoader : UnityEngine.MonoBehaviour
{
    // Start is called before the first frame update
    async UniTaskVoid Start()
    {
        Application.targetFrameRate = 60;
        AsyncOperationHandle<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> op;
        op = Addressables.LoadSceneAsync(SceneKey.LOADING, loadMode: LoadSceneMode.Single, false);
        await op;

        AD.Controller.Instance.Init(true);
        IAP.Controller.Instance.InitProduct();

        //wait for splash scene end
        await UniTask.Delay(2500);
        await op.Result.ActivateAsync();
    }

}
