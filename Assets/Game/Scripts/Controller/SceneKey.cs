﻿public class SceneKey
{
    public const string MENU = "MainScene";
    public const string GAME = "GameScene";
    public const string LOADING = "LoadingScene";
}