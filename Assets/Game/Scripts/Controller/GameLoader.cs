using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoader : MonoBehaviour
{
    // Start is called before the first frame update
    async UniTaskVoid Start()
    {

        LoadingScreen loadingScreen =(await Game.Pool.GameObjectSpawner.Instance.GetAsync("LoadingScreen")).GetComponent<LoadingScreen>();
        loadingScreen.Show();
        SceneLoader sceneLoader= new SceneLoader(SceneKey.MENU);
        sceneLoader.onSceneLoading += (progress) =>
        {
            loadingScreen.SetProgress(progress/2f);
        };
        sceneLoader.onScenePresented += () =>
        {
            loadingScreen.Hide();
            LoadMainScene();
        };

        float startTime = Time.time;
        float waitOpenAdTimeOut = 4;
        await UniTask.WaitUntil(() => RemoteConfigHandler.Instance.isReady &&
        ((!AD.Controller.Instance.IsAd || AD.Controller.Instance.IsOpenAdAvailable()) || (Time.time - startTime >= waitOpenAdTimeOut)));
        loadingScreen.SetProgress(1);
        sceneLoader.ActiveScene();

    }
    

    void LoadMainScene()
    {
        Debug.Log("LOAD HOME");
        UI.PanelManager.Create(typeof(HomePanel), panel =>
        {
            ((HomePanel)panel).SetUp();
        });
    }

  
}
