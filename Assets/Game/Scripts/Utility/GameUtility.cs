﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace GameUtility
{
    public static class GameUtility
    {
        public static void Log(object message)
        {
            Debug.Log(message.ToString());
        }
        public static void LogError(object message)
        {
            Debug.LogError(message.ToString());
        }
        public static void LogWarning(object message)
        {
            Debug.LogWarning(message.ToString());
        }
    }
}