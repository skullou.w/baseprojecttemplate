using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomePanel : UI.Panel
{
 
    public override void PostInit()
    {
        SetUp();
    }
    public void SetUp()
    {
        Show();
    }

    public override void Deactive()
    {
        base.Deactive();
    }
    public void Play()
    {
    }


    public void ShowReward()
    {
        AD.Controller.Instance.ShowRewardedAd("VITRISHOW", res =>
        {
            //if reward finished
            if (res)
            {
                //reward
            }

        });
    }
    public void ShowInterstitial()
    {
        AD.Controller.Instance.ShowInterstitial(() =>
        {
            // inter ad closed;
        });
    }

}
