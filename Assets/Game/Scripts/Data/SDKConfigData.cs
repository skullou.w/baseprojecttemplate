﻿using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;

[CreateAssetMenu(menuName ="SDK/Config")]
public class SDKConfigData : ScriptableObject
{
    public SDKIdConfig sdkIdConfig;
    public AdConfig adConfig;

    [System.Serializable]
    public struct SDKIdConfig
    {
        [Header ("ANDROID")]
        public ObscuredString admobOpenAdID_Android;
        public ObscuredString admobNativeAdID_Android;
        public ObscuredString adjustID_Android;

        [Header("IOS")]
        public ObscuredString admobOpenAdID_Ios;
        public ObscuredString admobNativeAdID_Ios;
        public ObscuredString adjustID_Ios;
        [Header("TEST")]
        public ObscuredString admobOpenAdID_Test;
        public ObscuredString admobNativeAdID_Test;
        //
        public bool useAdTest;
        public com.adjust.sdk.AdjustEnvironment adjustEnvironment;
    }
}