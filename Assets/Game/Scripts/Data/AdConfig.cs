﻿using UnityEngine;

[System.Serializable]
public class AdConfig 
{
    public bool skipAd = false;

    public int adStartAfterLevel = 2, interAdCoolDown = 45;
    public bool openAd = true;
}
