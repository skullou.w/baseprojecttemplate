using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Game.AssetLoader
{
    public class AddressableAssetLoader:IAssetLoader
    {
        private List<AsyncOperationHandle> operations = new List<AsyncOperationHandle>();
        public AddressableAssetLoader()
        {
        }

        public AssetRequest<TObject> Load<TObject>(string address) where TObject : Object
        {
            var addressableHandle = Addressables.LoadAssetAsync<TObject>(address);
            addressableHandle.WaitForCompletion();
            var request = new AssetRequest<TObject>();
            request.SetTask(UniTask.FromResult(addressableHandle.Result));
            request.SetResult(addressableHandle.Result);

            operations.Add(addressableHandle);
            return request;
        }

        public AssetRequest<TObject> LoadAsync<TObject>(string address) where TObject : Object
        {
            var addressableHandle = Addressables.LoadAssetAsync<TObject>(address);
            var handle = new AssetRequest<TObject>();
            var utcs = new UniTaskCompletionSource<TObject>();
            addressableHandle.Completed += x =>
            {
                handle.SetResult(x.Result);
                utcs.TrySetResult(x.Result);
            };

            handle.SetTask(utcs.Task);
            operations.Add(addressableHandle);

            return handle;
        }

        public void Release()
        {
        }
        public void ReleaseAll()
        {
            for(int i = 0; i < operations.Count; i++)
            {
                Addressables.Release(operations[i]);
            }
            operations.Clear();
        }
    }

    public interface IAssetLoader
    {
        AssetRequest<TObject> Load<TObject>(string address) where TObject : Object;

        AssetRequest<TObject> LoadAsync<TObject>(string address) where TObject : Object;

        void Release();
        void ReleaseAll();
    }

    public class AssetRequest
    {
        public AssetRequest()
        {
        }
    }
    public class AssetRequest<TObject> where TObject : Object
    {
        public TObject Result { get; private set; }
        public UniTask<TObject> Task { get; private set; }

        public void SetResult(TObject result)
        {
            Result = result;
        }

        public void SetTask(UniTask<TObject> task)
        {
            Task = task;
        }
    }
}
