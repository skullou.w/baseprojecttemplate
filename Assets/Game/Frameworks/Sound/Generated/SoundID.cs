
namespace Sound
{
	public static class SoundID
	{
		public const string Beam1 =  "Beam1";
		public const string Beam2 =  "Beam2";
		public const string Beam3 =  "Beam3";
		public const string Beam4 =  "Beam4";
		public const string Electric =  "Electric";
		public const string Electric2 =  "Electric2";
		public const string AcidExplosion =  "AcidExplosion";
		public const string ARROW =  "ARROW";
		public const string hehe =  "hehe";
	}
}