using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

public class Observer
{
    
    public static void Publish<T>(T unit)
    {
        MessageBroker.Default.Publish(unit);
    }
    public static void Subscribe<T>(System.Action<T> onAction)
    {
        MessageBroker.Default.Receive<T>().Subscribe(onAction);
    }
}

public class MessageArgs
{

}
