using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameUtility.Pooling
{
    public class PoolHandler:MonoBehaviour
    {
        public List<PoolComponent> pool = new List<PoolComponent>();
        private List<PoolComponent> busyPool = new List<PoolComponent>();
        
        [SerializeField]
        private GameObject[] prefab;
        Transform holder;
        int index = 0;
        public PoolComponent Get(int prefabIndex)
        {
            if (holder == null)
            {
                this.holder = transform;
                if(prefab==null || prefab.Length == 0)
                {
                    prefab = new GameObject[transform.childCount];
                    int index = 0;
                    foreach(Transform child in transform)
                    {
                        prefab[index++] = child.gameObject;
                    }
                }
            }
            if (pool.Count > 0)
            {
                PoolComponent result = pool[pool.Count - 1];

                pool.Remove(result);
                busyPool.Add(result);

                return result;
            }
            GameObject obj = GameObject.Instantiate(prefab[prefabIndex], holder) as GameObject;
            PoolComponent t = obj.AddComponent<PoolComponent>();
            t.gameObject.name += index++;
            busyPool.Add(t);
            t.OnInitialized(this);

            return t;
        }
        public void Release(PoolComponent poolComponent)
        {
            if (busyPool.Contains(poolComponent))
            {
                busyPool.Remove(poolComponent);
                pool.Add(poolComponent);
            }
        }
        public void Clear()
        {
            busyPool.Reverse();
            pool.AddRange(busyPool);
            busyPool.Clear();
        }
    }
}