﻿using UnityEngine;

namespace GameUtility.Pooling
{
    public class PoolComponent : MonoBehaviour
    {
        PoolHandler poolHandler;
        public void OnInitialized(PoolHandler poolHandler)
        {
            this.poolHandler = poolHandler;
        }
        protected void OnDestroy()
        {
            poolHandler.Release(this);
        }
        protected void OnDisable()
        {
            poolHandler.Release(this);
        }

    }
}