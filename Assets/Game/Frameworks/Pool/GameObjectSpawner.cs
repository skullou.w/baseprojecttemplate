﻿using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Pool
{
    public class GameObjectSpawner : SpawnerBase<PoolObject>
    {
        public static GameObjectSpawner Instance;
        public override void Start()
        {
            Instance = this;
            loader = new Game.AssetLoader.AddressableAssetLoader();
        }

        protected Dictionary<string, ObjectCollection> collections=new Dictionary<string, ObjectCollection>();
        public override async UniTask<PoolObject> GetAsync(string id)
        {
            ObjectCollection collection;
            if (!this.collections.ContainsKey(id))
            {
                collection = new ObjectCollection(loader.LoadAsync<Object>(id));
                this.collections.Add(id, collection);

            }
            else
            {
                collection = this.collections[id];
            }
            await collection.Load();

            return collection.Get();

        }
        public override void Get(string id,System.Action<PoolObject> onLoaded)
        {
            GetAsync(id).ContinueWith(onLoaded).Forget();
        }

        public override void Destroy()
        {
            foreach(var collection in collections.Values)
            {
                collection.ClearAll();
            }
            loader.ReleaseAll();

        }
        public override void ClearAll()
        {
            foreach (var collection in collections.Values)
            {
                collection.ClearAll();
            }
        }

    
    }

}