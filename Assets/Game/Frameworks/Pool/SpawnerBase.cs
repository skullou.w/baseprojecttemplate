using Cysharp.Threading.Tasks;
using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Game.Pool
{
    public abstract class SpawnerBase<T> :MonoBehaviour
    {

        protected Game.AssetLoader.IAssetLoader loader;

        public abstract void Start();

        public abstract  UniTask<T> GetAsync(string id);
        public abstract  void Get(string id, System.Action<PoolObject> onLoaded);

        public abstract void ClearAll();
        public abstract void Destroy();
    }

}