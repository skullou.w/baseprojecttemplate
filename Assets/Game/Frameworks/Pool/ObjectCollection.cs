﻿using Cysharp.Threading.Tasks;
using Game.AssetLoader;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Pool
{
    [System.Serializable]
    public class ObjectCollection
    {
        public List<PoolObject> pool = new List<PoolObject>();
        public List<PoolObject> inUsePool = new List<PoolObject>();
        public AssetRequest<Object> loadRequest;
        bool isReady = false;
        bool isLoading = false;
        public ObjectCollection(AssetRequest<Object> request)
        {
            loadRequest = request;
        }
       
        public async UniTask Load()
        {
            if (isLoading)
            {
                await UniTask.WaitUntil(() => !isLoading);
                return;
            }
            isLoading = true;
            try
            {
                await loadRequest.Task;
            }
            catch(System.Exception e)
            {
                Debug.LogError(e);
            }
            
            isReady = true;
            isLoading = false;
        }

        public void Add(PoolObject obj)
        {
            pool.Add(obj);
            obj.onReleased = Remove;
        }
        public void Remove(PoolObject obj)
        {
            pool.Add(obj);
            inUsePool.Remove(obj);
        }
        public PoolObject Get()
        {
            if (loadRequest.Result == null) return null;
            if (pool.Count == 0)
            {
                GameObject obj = ((GameObject)GameObject.Instantiate(loadRequest.Result));
                obj.name = loadRequest.Result.name + "_" + (pool.Count + inUsePool.Count);
                Add(obj.AddComponent<PoolObject>());
            }
            PoolObject readyObj = pool[0];
            readyObj.IsAvailable = false;

            pool.Remove(readyObj);
            inUsePool.Add(readyObj);

            return readyObj;
        }

        public void ClearAll()
        {
            for(int i=0;i<inUsePool.Count;i++)
            {
                inUsePool[i].gameObject.SetActive(false);
            }
        }
    }

}