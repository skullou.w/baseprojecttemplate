using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace DataManagement
{
    public class DataManager
    {
        public DataPack data;
        public static DataManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DataManager();
                }
                return instance;
            }
            set { instance = value; }
        }
        private static DataManager instance;


        public DataManager()
        {
            data = new DataPack();

            data.Save();
        }

        public static void Save()
        {
            Instance.data.Save();
        }
        [UnityEditor.MenuItem("File/Clear All")]
        static void  ClearSave()
        {
            PlayerPrefs.DeleteAll();
            Utilities.SaveUtility.ClearSave();
        }
    }

}