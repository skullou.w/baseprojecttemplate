﻿namespace DataManagement
{
    [System.Serializable]
    public class UserProperty : Data
    {
        public IntProperty intProperties;
        public StringProperty stringProperties;
        public FloatProperty floatProperties;


        public UserProperty()
        {
            intProperties = new IntProperty();
            stringProperties = new StringProperty();
            floatProperties = new FloatProperty();
        }
        public int GetValue(string key, int defaultValue = 0)
        {
            return intProperties.GetValue(key, defaultValue);
        }
        public string GetValue(string key, string defaultValue = null)
        {
            return stringProperties.GetValue(key, defaultValue);
        }
        public float GetValue(string key, float defaultValue = 0)
        {
            return floatProperties.GetValue(key, defaultValue);
        }
        public void SetValue(string key, int defaultValue = 0)
        {
            intProperties.SetValue(key, defaultValue);
        }
        public void SetValue(string key, string defaultValue = null)
        {
            stringProperties.SetValue(key, defaultValue);
        }
        public void SetValue(string key, float defaultValue = 0)
        {
            floatProperties.SetValue(key, defaultValue);
        }

        //
        public static byte[] key = { 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15 };
        public static string saveLocation = "userproperties";
        public override byte[] GetSecretKey()
        {
            return key;
        }

        public override string GetSaveLocation()
        {
            return saveLocation;
        }
    }

}