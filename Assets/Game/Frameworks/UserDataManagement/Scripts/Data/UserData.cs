using CodeStage.AntiCheat.ObscuredTypes;
using System;
using System.Collections;

namespace DataManagement
{
    [System.Serializable]
    public class UserData : Data
    {

        public ObscuredString Id { get; private set; }
        public string userName;

        public UserData()
        {
            Id = Guid.NewGuid().ToString();
        }
        public static byte[] key = { 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15 };
        public static string saveLocation = "userdata";
        public override byte[] GetSecretKey()
        {
            return key;
        }

        public override string GetSaveLocation()
        {
            return saveLocation;
        }
    }

   

}