﻿namespace DataManagement
{
    [System.Serializable]
    public class UserInventory : Data
    {
        public static byte[] key = { 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15, 0x16, 0x15 };
        public static string saveLocation = "userinventory";
        public override byte[] GetSecretKey()
        {
            return key;
        }

        public override string GetSaveLocation()
        {
            return saveLocation;
        }
    }

}