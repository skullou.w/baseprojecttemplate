﻿using CodeStage.AntiCheat.ObscuredTypes;
using System.Collections.Generic;

namespace DataManagement
{
    [System.Serializable]
    public class Property<T>
    {
        public Dictionary<string, T> data = new Dictionary<string, T>();

        public T GetValue(string key, T defaultValue)
        {
            if (!data.ContainsKey(key))
            {
                return defaultValue;
            }
            return data[key];
        }
        public void SetValue(string key, T value)
        {
            if (!data.ContainsKey(key))
            {
                data.Add(key, value);
            }
            else
            {
                data[key] = value;
            }
        }
    }
    public class IntProperty : Property<ObscuredInt>
    {
    }
    public class StringProperty : Property<ObscuredString>
    {
    }
    public class FloatProperty : Property<ObscuredFloat>
    {
    }
}