﻿namespace DataManagement
{
    public class DataPack:ISavable
    {
        public UserData userData;
        public UserInventory userInventory;
        public UserProperty userProperty;
        public DataPack()
        {
            userData = Load<UserData>(UserData.key, UserData.saveLocation);
            if (userData == null)
            {
                userData = new UserData();
            }
            userInventory = Load<UserInventory>(UserInventory.key, UserInventory.saveLocation);
            if (userInventory == null)
            {
                userInventory = new UserInventory();
            }
            userProperty = Load<UserProperty>(UserProperty.key, UserProperty.saveLocation);
            if (userProperty == null)
            {
                userProperty = new UserProperty();
            }
        }

        public static T Load<T>(byte [] key,string location)
        {
            return Utilities.SaveUtility.Instance.LoadFile<T>(key,location);
        }

        public void Save()
        {
            userData.Save();
            userInventory.Save();
            userProperty.Save();
        }
    }

}