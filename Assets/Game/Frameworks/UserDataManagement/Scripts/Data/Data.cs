﻿using System.IO;
using UnityEngine;

namespace DataManagement
{
    [System.Serializable]
    public abstract class Data : ISavable
    {
        // Key for reading and writing encrypted data.

        public void Save()
        {
            Utilities.SaveUtility.Instance.Save<Data>(this, GetSecretKey(), Path.Combine(Application.persistentDataPath, GetSaveLocation()));
        }


        public abstract byte[] GetSecretKey();

        public abstract string GetSaveLocation();
    }

}