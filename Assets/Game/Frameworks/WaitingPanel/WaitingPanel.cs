using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingPanel : UI.Panel
{
    private static UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> op;
    public static void Create(System.Action<UI.Panel> onDone)
    {
        if (Instance != null)
        {
            onDone?.Invoke(Instance);
            return;
        }
        UnityEngine.AddressableAssets.Addressables.InstantiateAsync("WaitingPanel", UI.PanelManager.Instance.transform).Completed += (op) =>
        {
            WaitingPanel.op = op;
            Instance = op.Result.GetComponent<WaitingPanel>();
            onDone?.Invoke(Instance);
        };
    }

    [SerializeField]
    private GameObject backBtn;
    private void OnDisable()
    {
        if (op.IsValid())
        {
            UnityEngine.AddressableAssets.Addressables.ReleaseInstance(op);
            Instance = null;
        }
    }
    public static WaitingPanel Instance;
    public override void PostInit()
    {
        Instance = this;
    }

    public void SetUp()
    {
        Show();
        Invoke(nameof(ShowBack), 5);
    }
    void ShowBack()
    {
        backBtn.SetActive(true);
    }
   

    public override void Hide()
    {
        GetComponent<PanelFadeAnimation>().Close(() => Deactive());
    }
}
