using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Game.Sheet
{
    [CustomEditor(typeof(GameConfigPackageSO))]
    public class GameConfigPackageEditor : Editor
    {
        string json;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GameConfigPackageSO target = (GameConfigPackageSO)this.target;

            if (GUILayout.Button("Create json"))
            {
                json = Newtonsoft.Json.JsonConvert.SerializeObject(target, new ObscuredValueConverter());
            }
            if (!string.IsNullOrEmpty(json))
            {
                GUILayout.TextArea(json, GUILayout.Height(100));
            }


        }
    }
}