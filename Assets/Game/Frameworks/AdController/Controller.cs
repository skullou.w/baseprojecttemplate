﻿using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace AD
{
    public class Controller:MonoBehaviour
    {
        public static Controller Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject obj = new GameObject("AD CONTROLLER");
                    DontDestroyOnLoad(obj);
                    instance = obj.AddComponent<AD.Controller>();
                }
                return instance;
            }

            set
            {
                if (instance == null)
                {
                    instance = value;
                }
            }

        }

        private const string REWARD_COUNT = "RewardCount";
        private const string INTERSTITIAL_COUNT = "InterstitialCount";

        public bool isBusy = false;
        private bool isAd = false;
        private  bool isInit;

        // if ad is active
        public bool IsAd { get => isAd; set => isAd = value; }

        private static Controller instance;
        List<IAdHandler> adHandler = new List<IAdHandler>();

        public delegate void OnNativeAdRefresh();
        public OnNativeAdRefresh onNativeAdRefresh;

      
        private float lastTimeShowInterstitialAd = 0;
        private float lastTimeShowOpenAd = 0;
        private float lastTimeShowRewardAd = 0;


        // config of the ad
        public AdConfig adConfig;
        public Controller()
        {
            
        }
        public void Init(bool isAd)
        {
            if (isInit) return;
            isInit = true;
            GameUtility.GameUtility.Log("AD INIT");
            adConfig = Resources.Load<SDKConfigData>("SdkConfig").adConfig;



            GameObject icObj = new GameObject("IronSourceController", typeof(IronSourceAdController));
            IronSourceAdController icAdCtr = icObj.GetComponent<IronSourceAdController>();
            icObj.transform.SetParent(transform);
            adHandler.Add(icAdCtr);
            //ko dung admob thi bo di
            GameObject admobObj = new GameObject("AdmobController", typeof(AdmobController));
            admobObj.transform.SetParent(transform);
            AdmobController admobAdCtr = admobObj.GetComponent<AdmobController>();
            adHandler.Add(admobAdCtr);

            foreach (IAdHandler adHandler in adHandler)
            {
                adHandler.Init();
            }
            this.IsAd = isAd;

            LoadOpenAd();
            LoadBanner();
            LoadInterstitial();
            LoadNativeAd();
        }
        //remove all ad
        public void RemoveAd()
        {
            IsAd = false;
            HideBanner();
            ReloadAllNativeAdBanner();
        }
        // detect if app is focused again, then show open ad
        private void OnApplicationPause(bool pause)
        {
            if (!pause)
            {
                Debug.Log("SHOW OPEN AD PAUSE");
                ShowOpenAd();
            }
        }

        #region reward ad
        public void ShowRewardedAd(string place, System.Action<bool> onRewared, bool canSkip = false)
        {
#if !UNITY_EDITOR
            // k cho spam gọi show QC tren editor
            if (Time.realtimeSinceStartup - lastTimeShowAd < 5) return;
#endif

            if (canSkip||adConfig.skipAd )
            {
                onRewared?.Invoke(true);
                return;
            }

            isBusy = true;
            lastTimeShowRewardAd = Time.realtimeSinceStartup;

            // hiện màn hình Loading AD
            WaitingPanel.Create((panel) =>
            {
                ((WaitingPanel)panel).SetUp();
            });

            //log event start Reward ở 1 nút nào đấy
            FirebaseAnalysticController.Instance.LogStartAd(place);
            System.GC.Collect();

            //
            foreach (IAdHandler adHandler in adHandler)
            {
                adHandler.ShowRewardedAd(async (result) =>
                 {
                     GameUtility.GameUtility.Log("AD REWARD :" + result);
                     if (result)
                     {
                         FirebaseAnalysticController.Instance.LogWatchAd(place);
                         PlayerPrefs.SetInt(REWARD_COUNT, PlayerPrefs.GetInt(REWARD_COUNT, 0) + 1);
                     }
                     await UniTask.Delay(500, ignoreTimeScale: true);
                     // return reward result
                     onRewared?.Invoke(result);
                     isBusy = false;
                     if (WaitingPanel.Instance != null)
                         WaitingPanel.Instance.Close();

                 }) ;
            }
        }
        #endregion

        #region interstitial ad
        public void ShowInterstitial(System.Action onClosed=null)
        {
            if (!IsAd || adConfig.skipAd || Time.time - lastTimeShowInterstitialAd < adConfig.interAdCoolDown)
            {
                onClosed?.Invoke();
                return;
            }
            isBusy = true;
            FirebaseAnalysticController.Instance.LogInterstitialStart();

            //// hiện màn hình Loading AD

            foreach (IAdHandler adHandler in adHandler)
            {
               adHandler.ShowInterstitial(
                   onShow:res =>
                    {
                        if (res)
                        {
                            PlayerPrefs.SetInt(INTERSTITIAL_COUNT, PlayerPrefs.GetInt(INTERSTITIAL_COUNT, 0)+1);
                            FirebaseAnalysticController.Instance.LogInterstitialFinish();
                        }
                    }, 
                   onClose:()=>
                   {
                       onClosed?.Invoke();
                       isBusy = false;
                       Debug.Log("INTER FINISH");
                   }
                );
            }
           
        }
        public void LoadInterstitial()
        {
            if (!IsAd || adConfig.skipAd) return;
            foreach (IAdHandler adHandler in adHandler)
            {
                adHandler.LoadInterstitial();
            }
        }
        #endregion

        #region banner ad
        public bool IsBannerLoaded()
        {
            foreach (IAdHandler adHandler in adHandler)
            {
                if (adHandler.IsBannerLoaded())
                {
                    return true;
                }
            }
            return false;
        }
        public void ShowBanner()
        {
            if (!IsAd || adConfig.skipAd) return;
            foreach (IAdHandler adHandler in adHandler)
            {
                adHandler.ShowBanner();
            }
        }
        public void HideBanner()
        {
            foreach (IAdHandler adHandler in adHandler)
            {
                adHandler.HideBanner();
            }
        }
        public void LoadBanner()
        {
            if (!IsAd) return;
            foreach (IAdHandler adHandler in adHandler)
            {
                adHandler.LoadBanner();
            }
        }
        #endregion

        #region open ad
        public void LoadOpenAd()
        {
            foreach (IAdHandler adHandler in adHandler)
            {
                adHandler.LoadOpenAd();
            }
        }
        public void ShowOpenAd()
        {
            Debug.Log("SHOW OPEN AD " + Time.time +" " + lastTimeShowOpenAd);
            if (!IsAd || adConfig.skipAd || isBusy) return;
            foreach (IAdHandler adHandler in adHandler)
            {
                adHandler.ShowOpenAd(res =>
                {
                    if (res)
                    {
                        lastTimeShowOpenAd = Time.time;
                        FirebaseAnalysticController.Instance.LogOpenAdStart();
                    }
                });
            }
        }
        public bool IsOpenAdAvailable()
        {
            foreach(IAdHandler adHandler in adHandler)
            {
                if (adHandler.IsOpenAdAvailable())
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region native ad
        public void LoadNativeAd()
        {
            Debug.Log("LOAD NATIVE AD");
            foreach(IAdHandler adHandler in adHandler)
            {
                adHandler.LoadNativeAd();
            }
        }
        public bool IsNativeAdAvailable()
        {
            foreach (IAdHandler adHandler in adHandler)
            {
                if (adHandler.IsNativeAdLoaded())
                {
                    return true;
                }
            }
            return false;
        }

        public object GetNativeAd()
        {
            foreach (IAdHandler adHandler in adHandler)
            {
                if (adHandler.IsNativeAdLoaded())
                {
                    return adHandler.GetCurrentNativeAd();
                }
            }
            LoadNativeAd();
            return null;
        }
       
        public void ReloadAllNativeAdBanner()
        {
            Debug.Log("RELOAD NATIVE AD");
            LoadNativeAd();
            onNativeAdRefresh?.Invoke();
        }

        #endregion
      


    }
}