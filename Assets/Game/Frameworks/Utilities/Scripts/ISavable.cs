﻿namespace DataManagement
{
    public interface ISavable
    {
        void Save();

    }

}