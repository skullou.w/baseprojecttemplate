﻿using Newtonsoft.Json;
using System.IO;
using System.Security.Cryptography;
using UnityEngine;

namespace Utilities
{
    public class SaveUtility
    {
        public static SaveUtility Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SaveUtility();
                }
                return instance;
            }
            set { instance = value; }
        }
        private static SaveUtility instance;
        FileStream dataStream;

        public SaveUtility()
        {
        }
        public static void ClearSave()
        {
            string[] files = Directory.GetFiles(Application.persistentDataPath);
            foreach(string file in files)
            {
                Debug.Log("FILE: " + file);
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
           
        }
      
        public bool Save<T>(T data, byte[] secretKey,string saveFile)
        {
            if (File.Exists(saveFile))
            {
                File.Delete(saveFile);
            }
            // Create new AES instance.
            Aes iAes = Aes.Create();

            // Update the internal key.
            //savedKey = iAes.Key;


            // Create a FileStream for creating files.
            dataStream = new FileStream(saveFile, FileMode.Create);

            // Save the new generated IV.
            byte[] inputIV = iAes.IV;

            // Write the IV to the FileStream unencrypted.
            dataStream.Write(inputIV, 0, inputIV.Length);
            //GameUtility.GameUtility.Log("save " + savedKey);
            //for (int i = 0; i < savedKey.Length; i++)
            //{
            //    GameUtility.GameUtility.Log(savedKey[i]);
            //}
            // Create CryptoStream, wrapping FileStream.
            CryptoStream iStream = new CryptoStream(
                    dataStream,
                    iAes.CreateEncryptor(secretKey, iAes.IV),
                    CryptoStreamMode.Write);

            // Create StreamWriter, wrapping CryptoStream.
            StreamWriter sWriter = new StreamWriter(iStream);

            // Serialize the object into JSON and save string.
            string jsonString = JsonConvert.SerializeObject(data, new ObscuredValueConverter());

            // Write to the innermost stream (which will encrypt).
            sWriter.Write(jsonString);

            // Close StreamWriter.
            sWriter.Close();

            // Close CryptoStream.
            iStream.Close();

            // Close FileStream.
            dataStream.Close();
            //PlayerPrefs.SetString("key", System.Convert.ToBase64String(savedKey));

            return true;
        }

        public T LoadFile<T>(byte[] secretKey, string saveFile)
        {
            // Does the file exist?
            if (File.Exists(saveFile))
            {
                try
                {
                    // Create FileStream for opening files.
                    dataStream = new FileStream(saveFile, FileMode.Open);

                    // Create new AES instance.
                    Aes oAes = Aes.Create();

                    // Create an array of correct size based on AES IV.
                    byte[] outputIV = new byte[oAes.IV.Length];

                    // Read the IV from the file.
                    dataStream.Read(outputIV, 0, outputIV.Length);

                    // Create CryptoStream, wrapping FileStream
                    //GameUtility.GameUtility.Log("load " + savedKey);
                    //for(int i = 0; i < savedKey.Length; i++)
                    //{
                    //    GameUtility.GameUtility.Log(savedKey[i]);
                    //}
                    CryptoStream oStream = new CryptoStream(
                           dataStream,
                           oAes.CreateDecryptor(secretKey, outputIV),
                           CryptoStreamMode.Read);

                    // Create a StreamReader, wrapping CryptoStream
                    StreamReader reader = new StreamReader(oStream);

                    // Read the entire file into a String value.
                    string text = reader.ReadToEnd();
                    // Always close a stream after usage.
                    reader.Close();
                    dataStream.Close();

                    // Deserialize the JSON data 
                    //  into a pattern matching the GameData class.
                    T data = JsonConvert.DeserializeObject<T>(text, new ObscuredValueConverter());
                    return data;
                }
                catch (System.Exception e)
                {
                    dataStream.Close();
                    Logger.LogError(e);
                    return default(T);
                }
            }
            return default(T);
        }
    }
}