﻿using Cysharp.Threading.Tasks;
using UnityEngine.AddressableAssets;

public class SceneLoader
{
    public delegate void OnScenePresented();
    public OnScenePresented onScenePresented;
    public delegate void OnSceneLoaded();
    public  OnSceneLoaded onSceneLoaded;
    public delegate void OnSceneLoading(float progress);
    public OnSceneLoading onSceneLoading;

    UnityEngine.ResourceManagement.ResourceProviders.SceneInstance scene;
    public bool isSceneLoaded;
    public SceneLoader(string sceneId)
    {
        LoadScene(sceneId);
    }
    
    public  void LoadScene(string sceneId)
    {
        isSceneLoaded = false;
        var op= Addressables.LoadSceneAsync(sceneId, UnityEngine.SceneManagement.LoadSceneMode.Single, false);
       
        op.Completed += (handle =>
         {
             scene = handle.Result;
             onSceneLoaded?.Invoke();
             isSceneLoaded = true;
         });
        CaculateProgress().Forget();
        async UniTaskVoid CaculateProgress()
        {
            while (!op.IsDone)
            {
                onSceneLoading?.Invoke(op.PercentComplete);
                await UniTask.Yield();
            }
        }

        

    }

    public async UniTask ActiveScene()
    {
        await scene.ActivateAsync();

        await UniTask.Delay(500);
        onScenePresented?.Invoke();
    }

}
