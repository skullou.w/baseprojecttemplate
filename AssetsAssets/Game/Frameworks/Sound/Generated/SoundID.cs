
namespace Sound
{
	public static class SoundID
	{
		/// <summary>
		/// CMT
		/// <summary>
		public const string Beam1 = "//Beam1";
		/// <summary>
		/// CMT
		/// <summary>
		public const string Beam2 = "//Beam2";
		/// <summary>
		/// CMT
		/// <summary>
		public const string Beam3 = "//Beam3";
		/// <summary>
		/// CMT
		/// <summary>
		public const string Beam4 = "//Beam4";
		/// <summary>
		/// CMT
		/// <summary>
		public const string Electric = "//Electric";
		/// <summary>
		/// CMT
		/// <summary>
		public const string Electric2 = "//Electric2";
		/// <summary>
		/// CMT
		/// <summary>
		public const string AcidExplosion = "//AcidExplosion";
		/// <summary>
		/// CMT
		/// <summary>
		public const string ARROW = "//ARROW";
	}
}